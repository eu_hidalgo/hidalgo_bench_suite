#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

HIDALGO_BENCH_HOSTS_DEFAULT=hidalgo_testbeds
HIDALGO_BENCH_RESULTS_DIR_DEFAULT=$PWD/hidBS

HIDALGO_BENCH_CMD=${1:-help}
HIDALGO_BENCH_HOSTS=${2:-$HIDALGO_BENCH_HOSTS_DEFAULT}
HIDALGO_BENCH_RESULTS_DIR=$(realpath ${3:-$HIDALGO_BENCH_RESULTS_DIR_DEFAULT})

current_dir=`dirname "$0"`
root_dir=`cd "${current_dir}/.."; pwd`
hidalgo_bench_ansible_dir=$root_dir/ansible

case $HIDALGO_BENCH_CMD in
    help)
	HIDALGO_BENCH_CMD_LIST=$(find $root_dir/ansible -name "hidalgo_bench_*.yaml" | xargs -I {} basename {} | \
				     sed -e "s/^hidalgo_bench_//" -e "s/.yaml//" | tr "\n" '|')
	HIDALGO_BENCH_TESTBEDS_LIST=$(ls $root_dir/ansible/spack-configs/site-scopes | xargs -I {} basename {} | tr "\n" '|')
	echo """Usage::

   $0 command [hosts_pattern [results_dir [ansible-extra-params]]]

Parameters:
- command={${HIDALGO_BENCH_CMD_LIST}help}
- hosts_pattern (default is \"$HIDALGO_BENCH_HOSTS_DEFAULT\")
  Supported testbeds: {$HIDALGO_BENCH_TESTBEDS_LIST}
- results_dir (default is \"$HIDALGO_BENCH_RESULTS_DIR_DEFAULT\")

Available commands:
- deploy       Deploys Spack
- install      Installs benchmark dependencies with Spack
- fetch_setup  Fetches information about the testbeds
- cleanup      Removes benchmark from the testbeds
"""
	;;
    *)
	# cd $root_dir/ansible
	ANSIBLE_ROLES_PATH=$ANSIBLE_ROLES_PATH:$hidalgo_bench_ansible_dir/core_scripts/ansible/roles \
			  ansible-playbook -i $hidalgo_bench_ansible_dir/inventory \
			  $hidalgo_bench_ansible_dir/hidalgo_bench_${HIDALGO_BENCH_CMD}.yaml \
			  --extra-vars "hidalgo_bench_results_dir=$HIDALGO_BENCH_RESULTS_DIR/{{inventory_hostname}}" \
			  -l $HIDALGO_BENCH_HOSTS
	;;
esac
