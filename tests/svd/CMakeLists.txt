############################################################
## @file
## @copyright (C) 2020
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <sergiy.gogolenko@gmail.com>
##
############################################################

project(svd_bench CXX C)
cmake_minimum_required(VERSION 3.5)
set(CMAKE_CXX_STANDARD 17)

# Look for CMake packages in local CMake folder as well
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake/Modules")

if(NOT CMAKE_BUILD_TYPE MATCHES Debug)
  set(CMAKE_BUILD_TYPE Release)
endif()
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

# find_package(MAGMA REQUIRED)
# add_executable(magma_bench magma_svd.cpp)
# target_link_libraries(magma_bench  ${MAGMA_LIBRARIES}) #LAPACK::LAPACK
# target_include_directories(magma_bench PUBLIC ${MAGMA_INCLUDE_DIR})

find_package(LAPACK)
if(LAPACK_FOUND AND BLAS_FOUND)
  add_executable(svd_bench svd_bench.cpp)
  target_link_libraries(svd_bench  ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES}) #LAPACK::LAPACK
  # target_include_directories(svd_bench PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
endif()
