#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`

############################################################
# Load Spack envs and compile Magma SVD subkernel
############################################################

. /lustre/hpe/ws10/ws10.3/ws/hpcgogol-hidBS/hidalgo_bench_suite/nvA100/.spack/share/spack/setup-env.sh
spack load cuda magma

cd $HBS_TEST_DIR && make

############################################################
# Manually install TF
# For ROCm:
# - https://github.com/ROCmSoftwarePlatform/tensorflow-upstream
# - https://rocmdocs.amd.com/en/latest/Deep_learning/Deep-learning.html#tensorflow
############################################################

ws_allocate hidBS_svd
HBS_SVD_TMPDIR=`ws_find hidBS_svd`

cd $HBS_SVD_TMPDIR

export PATH="/sw/general/x86_64/tools/conda/miniconda3/4.10.3/bin/:$PATH"
export CONDA_ENVS_PATH=$HBS_SVD_TMPDIR/conda/envs
export CONDA_PKGS_DIRS=$HBS_SVD_TMPDIR/conda/pkgs
export CUDNN_PREFIX=$HBS_SVD_TMPDIR/cudnn/8.2.2.26/cuda11.4
export TMPDIR=$HBS_SVD_TMPDIR/tmp
mkdir -p $CONDA_ENVS_PATH
mkdir -p $CONDA_PKGS_DIRS
mkdir -p $TMPDIR

. activate
conda create -n hidBS_svd python==3.8.10
conda activate hidBS_svd

export ONE_PROXY="http://127.0.0.1:8118"; export http_proxy="$ONE_PROXY"; export https_proxy="$ONE_PROXY"; export HTTP_PROXY="$ONE_PROXY"; export HTTPS_PROXY="$ONE_PROXY"
wget https://developer.download.nvidia.com/compute/redist/cudnn/v8.2.2/cudnn-11.4-linux-x64-v8.2.2.26.tgz
mkdir -p $CUDNN_PREFIX && tar -xvzf ./cudnn-11.4-linux-x64-v8.2.2.26.tgz -C $CUDNN_PREFIX --strip 1
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$CUDNN_PREFIX/lib64"

pip install tensorflow-gpu==2.4.3
unset ONE_PROXY; unset http_proxy; unset https_proxy; unset HTTP_PROXY; unset HTTPS_PROXY

ln -s "$(ldconfig -p | grep 'libcusolver.so$' | tr ' ' '\n' | grep /)" $(python -c "import tensorflow.python as x; print(x.__path__[0])")/libcusolver.so.10
