#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`
. $HBS_TEST_DIR/../common/bin/functions/time_functions.sh

HBS_METRIC_FUNC=${1:-time_perf}
NREPEATS=${2:-5}
HBS_DATASET_MAX=${3:-40000}

# HBS_TESTBED=nvA100
HBS_TESTBED=$(basename $(dirname $(spack location -r)))

HBS_SUBKERNEL=TensorFlow
echo "HBS_SUBKERNEL=$HBS_SUBKERNEL"
for HBS_DATASET in 1000 2000 4000 8000 16000 32000 40000; do
    echo "  -- dataset=$HBS_DATASET"
    if [ $HBS_DATASET -le $HBS_DATASET_MAX ]; then
	for i in $(seq 1 $NREPEATS); do
	    hbs_$HBS_METRIC_FUNC python3 $HBS_TEST_DIR/src/svd_bench_tf.py $HBS_DATASET
	done
    fi
done
