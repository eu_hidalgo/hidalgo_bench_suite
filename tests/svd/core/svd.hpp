#pragma once

#include "matrix.hpp"

#include <cassert>
#include <limits>
#include <cstdlib>
#include <random>

namespace lapack {
  template <typename Ordering>
  constexpr int ordering_code = 'C';
  template<>
  constexpr int ordering_code<hpc12::row_major> = 'R';

  namespace linalg {
    template <typename T, typename Ordering, typename Allocator >
    std::vector<T> svd(hpc12::matrix<T, Ordering, Allocator> a, bool full_matrices=false, bool compute_uv=false)
    {
      // Specifies options for computing all or part of the matrix U/V^T:
      // = 'A':  all M columns of U are returned in array U:
      // = 'S':  the first min(m,n) columns of U (the left singular
      // 	  vectors) are returned in the array U;
      // = 'O':  the first min(m,n) columns of U (the left singular
      // 	  vectors) are overwritten on the array A;
      // = 'N':  no columns of U (no left singular vectors) are
      // 	  computed.
      std::vector<T> s(std::min(a.size1(), a.size2()));
      hpc12::matrix<T, Ordering, Allocator> U(1,1), Vt(1,1);
      if(!compute_uv) {
	// lapack::dgesvd( int const& n, int const& nrhs,
	// 		    double *A, int const& ia, int const& ja, int *desca, int* ipiv,
	// 		    double *B, int const& ib, int const& jb, int *descb, int *info);
	std::vector<T> superb{};
	dgesvd( ordering_code<Ordering> ,
		'N',
		'N',
		a.size1(),
		a.size2(),
		a.data(),
		a.size1(),
		s.data(),
		U.data(),
		U.size1(),
		Vt.data(),
		Vt.size1(),
		superb.data() );
      }
      return s;
    }
  }

  namespace random {
    template <typename T, typename Ordering = hpc12::column_major, typename Allocator = hpcse::aligned_allocator<T,64> >
    hpc12::matrix<T, Ordering, Allocator> uniform( std::pair<std::size_t, std::size_t> shape, T minval=T{0}, T maxval=T{1}, unsigned int seed = 0)
    {
      hpc12::matrix<T, Ordering, Allocator> A(shape.first, shape.second);

      std::mt19937 gen(seed);
      std::uniform_real_distribution<T> dis(minval, maxval);

      // Fill the local matrix with random elements
      for (int j{}; j < shape.second; ++j)
	for (int i{}; i < shape.first; ++i)
	  A(i,j) = dis(gen);
      return A;
    }
  }
}
