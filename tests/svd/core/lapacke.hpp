#pragma once

#include <vector>

#ifdef WITH_MKL
#include "mkl_lapacke.h"
// MKL_INT m = M, n = N, lda = LDA, ldu = LDU, ldvt = LDVT, info;
// info = LAPACKE_dgesvd( LAPACK_ROW_MAJOR, 'A', 'A', m, n, a, lda,
//                 s, u, ldu, vt, ldvt, superb );
namespace lapack {
  const auto dgesvd = LAPACKE_dgesvd;
  using int_t = MKL_INT;
  using size_t = MKL_INT;
}
#else
extern "C" {
  // http://www.netlib.org/lapack/explore-html/d1/d7e/group__double_g_esing_ga84fdf22a62b12ff364621e4713ce02f2.html
  void dgesvd_( char* jobu, char* jobvt, int* m, int* n, double* a,
		int* lda, double* s, double* u, int* ldu, double* vt, int* ldvt,
		double* work, int* lwork, int* info );
}
namespace lapack {
  using int_t = int;
  using size_t = int;

  int_t dgesvd( int  	matrix_layout,
		char  	jobu,
		char  	jobvt,
		int_t     m,
		int_t     n,
		double*   a,
		int_t     lda,
		double*   s,
		double*   u,
		int_t     ldu,
		double*   vt,
		int_t     ldvt,
		double* superb ) {
    int_t info{};

    // compute and allocate optimal work space
    double lwork_optimal{};
    int_t lwork{-1};
    dgesvd_( &jobu, &jobvt, &m, &n, a,
	     &lda, s, u, &ldu, vt, &ldvt,
	     &lwork_optimal, &lwork, &info );
    lwork = static_cast<int>(lwork_optimal);
    std::vector<double> work(lwork);

    // perform SVD
    dgesvd_( &jobu, &jobvt, &m, &n, a,
	     &lda, s, u, &ldu, vt, &ldvt,
	     work.data(), &lwork, &info );
    return info;
  }
}
#endif
