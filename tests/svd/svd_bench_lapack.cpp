#include "core/lapacke.hpp"
#include "core/svd.hpp"

int main(int argc, char** argv)
{
  std::random_device rd{};  // produces the seeds for the random number engines
  auto A = lapack::random::uniform<double>({100, 100}, -100., 100., rd());
  auto S = lapack::linalg::svd(A);
  for(const auto a : S)
    std::cout << a << " ";
  std::cout << std::endl;

  return 0;
}
