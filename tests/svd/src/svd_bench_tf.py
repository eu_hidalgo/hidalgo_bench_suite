############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

import sys
import tensorflow as tf
import numpy as np
import time

class BENCH_REPORT_TIMING(object):
    def __init__(self, func_name = 'tf'):
        self.func_name = func_name

    def __enter__(self):
        self.start = time.perf_counter_ns()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time.perf_counter_ns()
        print("BENCH:{}:{}:tf".format(1, 1e-9*(self.end - self.start)), file=sys.stderr)

if __name__=="__main__":
    size = 4000 if len(sys.argv) == 1 else int(sys.argv[1])

    with BENCH_REPORT_TIMING('uniform') as time_init:
        a = tf.random.uniform( (size, size) , minval=-100., maxval=100., dtype=tf.dtypes.float32, seed=None, name=None )
    with BENCH_REPORT_TIMING('svd') as time_svd:
        # s, u, v = svd(a)
        s = tf.linalg.svd(a, full_matrices=False, compute_uv=False, name=None)
