# - Find MAGMA library
# This module finds an installed MAGMA library, a matrix algebra library
# similar to LAPACK for GPU and multicore systems
# (see http://icl.cs.utk.edu/magma/).
#
# This module sets the following variables:
#  MAGMA_FOUND - set to true if the MAGMA library is found.
#  MAGMA_LIBRARIES - list of libraries to link against to use MAGMA
#  MAGMA_INCLUDE_DIR - include directory

IF(NOT MAGMA_FOUND)

  include(FindPackageHandleStandardArgs)

  SET(MAGMA_LIBRARIES)
  SET(MAGMA_INCLUDE_DIR)

  FIND_LIBRARY(MAGMA_LIBRARIES magma /usr/local/magma/lib)
  FIND_PATH(MAGMA_INCLUDE_DIR magma.h /usr/local/magma/include)

  IF (MAGMA_LIBRARIES)
    SET(MAGMA_FOUND TRUE)
  ELSE (MAGMA_LIBRARIES)
    SET(MAGMA_FOUND FALSE)
  ENDIF (MAGMA_LIBRARIES)

  # include(FindPackageHandleStandardArgs)
  # # handle the QUIETLY and REQUIRED arguments and set LIBOSRM_FOUND to TRUE
  # # if all listed variables are TRUE
  # find_package_handle_standard_args(MAGMA DEFAULT_MSG
  #   MAGMA_LIBRARY_DIRS
  #   MAGMA_CXXFLAGS
  #   MAGMA_LIBRARIES
  #   MAGMA_DEPENDENT_LIBRARIES
  #   MAGMA_INCLUDE_DIR)
ENDIF(NOT MAGMA_FOUND)
