#include <stdio.h>
#include <stdlib.h>

// #include "cublas_v2.h"     // if you need CUBLAS v2, include before magma.h
#include "magma.h"
#include "magma_lapack.h"  // if you need BLAS & LAPACK

/* #undef HIDBS_USE_SVD_DOUBLE */

/* #ifdef HIDBS_USE_SVD_DOUBLE */
/* typedef double magma_fpoint; */
/* #define magma_xGESVD magma_dgesvd */
/* #define magma_xMalloc_cpu magma_dmalloc_cpu */
/* #endif */

/* #ifndef HIDBS_USE_SVD_DOUBLE */
typedef float magma_fpoint;
#define magma_xGESVD magma_sgesvd
#define magma_xMalloc_cpu magma_smalloc_cpu
/* #endif */

// ------------------------------------------------------------
// Replace with your code to initialize the A matrix.
// This simply initializes it to random values.
// Note that A is stored column-wise, not row-wise.
//
// m   - number of rows,    m >= 0.
// n   - number of columns, n >= 0.
// A   - m-by-n array of size lda*n.
// lda - leading dimension of A, lda >= m.
//
// When lda > m, rows (m, ..., lda-1) below the bottom of the matrix are ignored.
// This is helpful for working with sub-matrices, and for aligning the top
// of columns to memory boundaries (or avoiding such alignment).
// Significantly better memory performance is achieved by having the outer loop
// over columns (j), and the inner loop over rows (i), than the reverse.
void dfill_matrix(magma_int_t m, magma_int_t n, magma_fpoint *A, magma_int_t lda )
{
#define A(i_, j_) A[ (i_) + (j_)*lda ]

  magma_int_t i, j;
  for (j=0; j < n; ++j) {
    for (i=0; i < m; ++i) {
      A(i,j) = rand() / ((magma_fpoint) RAND_MAX);
    }
  }

#undef A
}

// ------------------------------------------------------------
// Solve A * X = B, where A and X are stored in CPU host memory.
// Internally, MAGMA transfers data to the GPU device
// and uses a hybrid CPU + GPU algorithm.
void cpu_interface( magma_int_t n )
{
  magma_fpoint *A=NULL, *s=NULL;
  magma_fpoint U, VT, *work=NULL;
  magma_int_t lda  = n;
  magma_int_t info = 0;

  magma_int_t lwork = -1;
  magma_int_t ldu = 1, ldvt = 1;
  magma_fpoint d_lwork = 0;

  // get workspace size
  magma_xGESVD( MagmaNoVec, MagmaNoVec,
		n, n, A, lda, s,
		&U, ldu, &VT, ldvt,
		&d_lwork, lwork,
		&info );
  if (info != 0) {
    fprintf( stderr, "magma_dgesvd failed with info=%d\n", info );
  }
  lwork = (magma_int_t) d_lwork;

  // magma_*malloc_cpu routines for CPU memory are type-safe and align to memory boundaries,
  // but you can use malloc or new if you prefer.
  magma_xMalloc_cpu( &A, lda*n );
  magma_xMalloc_cpu( &s, n );
  magma_xMalloc_cpu( &work, lwork );
  if (A == NULL || s == NULL || work == NULL) {
    fprintf( stderr, "malloc failed\n" );
    goto cleanup;
  }

  // Initialize A
  dfill_matrix( n, n, A, lda );

  magma_xGESVD( MagmaNoVec, MagmaNoVec,
		n, n, A, lda, s,
		&U, ldu, &VT, ldvt,
		&d_lwork, -1,
		&info );
  if (info != 0) {
    fprintf( stderr, "magma_dgesvd failed with info=%d\n", info );
  }

  // TODO: use result in X
 cleanup:
  magma_free_cpu( A );
  magma_free_cpu( s );
  magma_free_cpu( work );
}

// ------------------------------------------------------------
int main( int argc, char** argv )
{
  magma_init();

  if (argc < 1) {
    printf( "specify matrix dimentions\n" );
    return EXIT_FAILURE;
  }

  magma_int_t n = atoi(argv[1]); // 40000
  cpu_interface( n );

  magma_finalize();
  return 0;
}
