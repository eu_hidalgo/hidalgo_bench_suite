///////////////////////////////////////////////////////////////////////////////
// @file
// @copyright (C) 2021
//    All rights reserved.
//
// Use, modification, and distribution is subject to the license.
//
// @author Sergiy Gogolenko <gogolenko@hlrs.de>
///////////////////////////////////////////////////////////////////////////////
// Computes the durations/distances of the fastest routes between all pairs of
// the supplied coordinates.
///////////////////////////////////////////////////////////////////////////////

#include <fstream>
#include <sstream>
#include <iostream>

#include <string>
#include <vector>
#include <utility>
// #include "csv.h"

#include "osrm/engine_config.hpp"
#include "osrm/json_container.hpp"
#include "osrm/table_parameters.hpp"

#include "osrm/osrm.hpp"
#include "osrm/status.hpp"

#ifdef DEBUG
#define DEBUG_MSG(text) std::cerr << text
#else
#define DEBUG_MSG(text) {}
#endif

// TODO: replace with LikWid: https://github.com/RRZE-HPC/likwid/blob/master/doc/applications/likwid-perfctr.md
#include "timing.h"

// This function translates C-string to character.
// If input is a const-string, the transformation is performed in compile-time.
// Credit to:
// https://stackoverflow.com/questions/16388510/evaluate-a-string-with-a-switch-in-c
constexpr unsigned int cstr_to_int(const char *str, int h = 0) {
  return !str[h] ? 5381 : (cstr_to_int(str, h + 1) * 33) ^ str[h];
}

bool compute_table( const std::vector<std::pair<double, double>> &latlons,
		    const std::string &routes_file = "map.osrm",
		    const char *table_type = "distances" ) {
  // table_type: "distances" corresponds to distances matrix,
  // it can be changed to "durations"

  // Configure based on a .osrm base path, and no datasets in shared mem from
  // osrm-datastore
  osrm::EngineConfig config;
  config.storage_config = {routes_file};
  config.use_shared_memory = false;

  // Select routing speed up techniques:
  // - MLD: Multi-Level Dijkstra (requires extract+partition+customize
  // pre-processing)
  // - CH: Contraction Hierarchies(requires extract+contract pre-processing)
  config.algorithm = osrm::EngineConfig::Algorithm::CH;

  // Routing machine with several services (including Table)
  const osrm::OSRM osrm{config};

  osrm::TableParameters params;
  for (const auto &latlon : latlons)
    params.coordinates.push_back({osrm::util::FloatLongitude{latlon.second},
                                  osrm::util::FloatLatitude{latlon.first}});

  // define annotations type (Duration, Distance, or All)
  switch (cstr_to_int(table_type)) {
  case cstr_to_int("distances"):
    params.annotations = osrm::TableParameters::AnnotationsType::Distance;
    break;
  case cstr_to_int("durations"):
    params.annotations = osrm::TableParameters::AnnotationsType::Duration;
    break;
  default:
    return false;
  }

  // Response is in JSON format
  osrm::engine::api::ResultT result = osrm::json::Object();

  // Execute routing request, this does the heavy lifting
  const auto status = osrm.Table(params, result);
  auto &json_result = result.get<osrm::json::Object>();

  if (status == osrm::Status::Ok) {
    DEBUG_MSG("Table service succeeded\n");
    // const auto &distance_matrix =
    json_result.values.at(table_type).get<osrm::json::Array>().values;
    DEBUG_MSG("Table dims: [" << json_result.values.at(table_type).get<osrm::json::Array>().values.size() << "x"
	      << json_result.values.at(table_type).get<osrm::json::Array>().values.size() << "]\n");

  } else if (status == osrm::Status::Error) {
    DEBUG_MSG("Code:    "
              << json_result.values["code"].get<osrm::json::String>().value
              << "\n");
    DEBUG_MSG("Message: "
              << json_result.values["message"].get<osrm::json::String>().value
              << "\n");
    return false;
  }
  return true;
}

int main(int argc, const char* argv[]) {
  if (argc < 2) {
    std::cerr
        << "Usage: " << argv[0]
        << " input-route-network.osrm input-locations.csv > output-matrix.txt";
    return EXIT_FAILURE;
  }
  // // no input-route-network.osrm
  // if(!std::filesystem::exists(argv[1]))
  //   return -2;

  // Read names from locations and their coordinates from CSV
  std::vector<std::string> location_names{};
  std::vector<std::pair<double, double>> latlons{};
  {
    std::string line;
    std::string name;
    float lon;
    float lat;

    std::ifstream file{argv[2]};
    while(file.good()) {
      std::getline(file, line);
      std::stringstream ss{line};
      std::getline( ss, name, ',' );
      if(!name.empty()) {
	location_names.push_back(name);
	std::string item;
	std::getline( ss, item, ',' );
	lat = std::atof(item.c_str());
	std::getline( ss, item, ',' );
	lon = std::atof(item.c_str());
	latlons.push_back({lat, lon});
	DEBUG_MSG(name << " at <" << lon << "," << lat << ">" << std::endl);
      }
    }
  }

  if (!compute_table(latlons, argv[1]))  // Compute distance matrix from coordinates
    return EXIT_FAILURE;
  return EXIT_SUCCESS;
}
