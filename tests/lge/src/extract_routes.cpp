///////////////////////////////////////////////////////////////////////////////
// @file
// @copyright (C) 2021
//    All rights reserved.
//
// Use, modification, and distribution is subject to the license.
//
// @author Sergiy Gogolenko <gogolenko@hlrs.de>
///////////////////////////////////////////////////////////////////////////////
// Computes the durations/distances of the fastest routes between all pairs of
// the supplied coordinates.
///////////////////////////////////////////////////////////////////////////////

#include <fstream>
#include <sstream>
#include <iostream>

#include <string>
#include <vector>
#include <utility>
// #include "csv.h"

#include "osrm/engine_config.hpp"
#include "osrm/json_container.hpp"
#include "osrm/table_parameters.hpp"

#include "osrm/osrm.hpp"
#include "osrm/status.hpp"

#include <limits>
#include <memory>

#ifdef DEBUG
#define DEBUG_MSG(text) std::cerr << text
#else
#define DEBUG_MSG(text) {}
#endif

#include "timing.h"

/*
#include <cstdlib>
#include <tbb/blocked_range.h>
#include <tbb/parallel_for.h>
#include <tbb/task_arena.h>
#include <tbb/task_group.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/global_control.h>

void set_num_threads() {
  int nthreads = tbb::task_scheduler_init::default_num_threads();
  const char* cnthreads = getenv("TBB_NUM_THREADS");
  if (cnthreads) nthreads = std::max(1, atoi(cnthreads));
  printf("Using %d threads\n", nthreads);
  tbb::task_group group;
  tbb::task_arena arena(nthreads, 1);
  tbb::global_control control(tbb::global_control::max_allowed_parallelism, nthreads);
  omp_set_num_threads(nthreads);
}
*/

// This function translates C-string to character.
// If input is a const-string, the transformation is performed in compile-time.
// Credit to:
// https://stackoverflow.com/questions/16388510/evaluate-a-string-with-a-switch-in-c
constexpr unsigned int cstr_to_int(const char *str, int h = 0) {
  return !str[h] ? 5381 : (cstr_to_int(str, h + 1) * 33) ^ str[h];
}

bool compute_table( float* dist_matr, int& n, 
		    const std::vector<std::pair<double, double>> &latlons,
		    const std::string &routes_file = "map.osrm",
		    const char *table_type = "distances" ) {
  // table_type: "distances" corresponds to distances matrix,
  // it can be changed to "durations"
  
  // Configure based on a .osrm base path, and no datasets in shared mem from
  // osrm-datastore
  osrm::EngineConfig config;
  config.storage_config = {routes_file};
  config.use_shared_memory = false;

  // Select routing speed up techniques:
  // - MLD: Multi-Level Dijkstra (requires extract+partition+customize
  // pre-processing)
  // - CH: Contraction Hierarchies(requires extract+contract pre-processing)
  config.algorithm = osrm::EngineConfig::Algorithm::CH;

  // Routing machine with several services (including Table)
  const osrm::OSRM osrm{config};

  osrm::TableParameters params;
  for (const auto &latlon : latlons)
    params.coordinates.push_back({osrm::util::FloatLongitude{latlon.second},
                                  osrm::util::FloatLatitude{latlon.first}});

  // define annotations type (Duration, Distance, or All)
  switch (cstr_to_int(table_type)) {
  case cstr_to_int("distances"):
    params.annotations = osrm::TableParameters::AnnotationsType::Distance;
    break;
  case cstr_to_int("durations"):
    params.annotations = osrm::TableParameters::AnnotationsType::Duration;
    break;
  default:
    return false;
  }

  // Response is in JSON format
  osrm::engine::api::ResultT result = osrm::json::Object();

  // Execute routing request, this does the heavy lifting
  const auto status = osrm.Table(params, result);
  auto &json_result = result.get<osrm::json::Object>();

  if (status == osrm::Status::Ok) {
    DEBUG_MSG("Table service succeeded\n");
    const auto &distance_matrix =
      json_result.values.at(table_type).get<osrm::json::Array>().values;
    DEBUG_MSG("Table dims: [" << json_result.values.at(table_type).get<osrm::json::Array>().values.size() << "x"
	      << json_result.values.at(table_type).get<osrm::json::Array>().values.size() << "]\n");

    n = distance_matrix.size();
    unsigned i{0};
    for (const auto &distance_array : distance_matrix) {
      unsigned j{0};
      for (const auto &distance_entry : distance_array.get<osrm::json::Array>().values) {
        dist_matr[i*n + j] = distance_entry.is<osrm::json::Null>() ?	\
	  std::numeric_limits<float>::max() : distance_entry.get<osrm::json::Number>().value;
	// if(i != j) std::cout << n << "???" << distance_entry.is<osrm::json::Null>() << ":"<< dist_matr[i*n + j] << std::endl;
        ++j;
      }
      ++i;
    }
  } else if (status == osrm::Status::Error) {
    DEBUG_MSG("Code:    "
              << json_result.values["code"].get<osrm::json::String>().value
              << "\n");
    DEBUG_MSG("Message: "
              << json_result.values["message"].get<osrm::json::String>().value
              << "\n");
    return false;
  }
  return true;
}

// std::vector<std::pair<int, int>> edege_list
template<typename T>
bool triangle_pruning(T* routes, bool* graph, int n, double triangle_factor=0.95, int threads = 0) {
// #pragma omp parallel for schedule(static, 1) num_threads(threads)
#pragma omp parallel for
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j) {
      if(i != j) {
	T route_dist = routes[i*n + j] / triangle_factor;
	int k{};
	for (k = 0; k < n; ++k)
	  // if(k != i && k !=j && routes[i*n + k] + routes[k*n + j] <= route_dist) {
	  if(k != i && k !=j && routes[i*n + k] + routes[j*n + k] <= route_dist) {
	    break;
	  }
	if(k == n)
	  graph[i*n + j] = true;// edege_list.push_back({i,j});
      }
    }
  return true;
}

int main(int argc, const char* argv[]) {
  if (argc < 2) {
    std::cerr
      << "Usage: " << argv[0]
      << " input-route-network.osrm input-locations.csv > output-matrix.txt";
    return EXIT_FAILURE;
  }
  // // no input-route-network.osrm
  // if(!std::filesystem::exists(argv[1]))
  //   return -2;

  // Read names from locations and their coordinates from CSV
  std::vector<std::string> location_names{};
  std::vector<std::pair<double, double>> latlons{};
  {
    std::string line;
    std::string name;
    float lon;
    float lat;

    std::ifstream file{argv[2]};
    while(file.good()) {
      std::getline(file, line);
      std::stringstream ss{line};
      std::getline( ss, name, ',' );
      if(!name.empty()) {
	location_names.push_back(name);
	std::string item;
	std::getline( ss, item, ',' );
	lat = std::atof(item.c_str());
	std::getline( ss, item, ',' );
	lon = std::atof(item.c_str());
	latlons.push_back({lat, lon});
	DEBUG_MSG(name << " at <" << lon << "," << lat << ">" << std::endl);
      }
    }
  }

  int n{};
  auto dist_matr = std::make_unique<float[]>(latlons.size()*latlons.size());

  // Compute distance matrix from coordinates
  BENCH_CALL_TIMING( if(!compute_table(dist_matr.get(), n, latlons, argv[1])) { return EXIT_FAILURE; } )

  // Write results in CSV format to stdout  
  {
    auto route_graph = std::make_unique<bool[]>(latlons.size()*latlons.size());
    std::fill(route_graph.get(), route_graph.get() + n*n, false);
    BENCH_CALL_TIMING( triangle_pruning(dist_matr.get(), route_graph.get(), latlons.size(), 0.95, 2) );
    
    // if (argc > 2) {
    //   int n = latlons.size();
    //   for(int i = 0; i < n; ++i) {
    // 	for(int j = 0; j < n; ++j)
    // 	  if(route_graph[i*n + j])
    // 	    std::cout << location_names[i] << "," << location_names[j] << "," << dist_matr[i*n + j] << std::endl;
    //   }
    // }
  }
  return EXIT_SUCCESS;
}
