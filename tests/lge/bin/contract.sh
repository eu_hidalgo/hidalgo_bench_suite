#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`
. $HBS_TEST_DIR/../common/bin/functions/time_functions.sh

HBS_METRIC_FUNC=${1:-time_perf}
HBS_NCORES=${2:-$(nproc)}
HBS_DATASET=${3:-africa/south-sudan}
OSRM_PROFILE=${4:-car}

OSM_FILE_BASENAME=map
OSRM_PROFILE_SCRIPT=$(spack location -i libosrm)/share/osrm/profiles/$OSRM_PROFILE.lua

HBS_TESTBED=$(basename $(dirname $(spack location -r)))

WORK_DIR=$HBS_TEST_DIR/data/${HBS_DATASET}
cd ${WORK_DIR}

HBS_SUBKERNEL=contract_$OSRM_PROFILE
hbs_$HBS_METRIC_FUNC osrm-contract -t $HBS_NCORES $WORK_DIR/$OSM_FILE_BASENAME.osrm
