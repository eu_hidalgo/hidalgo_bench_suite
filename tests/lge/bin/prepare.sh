#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################
HBS_LGE_GEO_REGION=europe/ukraine

# old_dir=$(pwd)
current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`

LGE_BUILD_DIR=$HBS_TEST_DIR/_build
mkdir $LGE_BUILD_DIR && cd $LGE_BUILD_DIR && cmake ../src && make

$HBS_TEST_DIR/bin/fetch_osm.sh $HBS_LGE_GEO_REGION

# for GEO_REGION in africa/south-sudan africa
# do
#     ../bin/fetch_osm.sh $GEO_REGION
#     ../bin/osm2osrm.sh  $GEO_REGION
# done

# cd $old_dir
