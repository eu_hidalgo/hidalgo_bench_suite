#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`
. $HBS_TEST_DIR/../common/bin/functions/time_functions.sh

GEO_REGION=${1:-africa/south-sudan}
OSRM_PROFILE=${2:-car}
OSM_FILE_BASENAME=map
OSRM_PROFILE_SCRIPT=$(spack location -i libosrm)/share/osrm/profiles/$OSRM_PROFILE.lua

HBS_DATASET=$GEO_REGION

WORK_DIR=$HBS_TEST_DIR/data/${HBS_DATASET}
cd ${WORK_DIR}

osrm-extract -p $OSRM_PROFILE_SCRIPT $WORK_DIR/$OSM_FILE_BASENAME.osm.pbf
# osrm-partition $WORK_DIR/$OSM_FILE_BASENAME.osrm > $HBS_OUTPUT_DIR/osrm-partition.log
# osrm-customize $WORK_DIR/$OSM_FILE_BASENAME.osrm > $HBS_OUTPUT_DIR/osrm-customize.log
osrm-contract $WORK_DIR/$OSM_FILE_BASENAME.osrm
