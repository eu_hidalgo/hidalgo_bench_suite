#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################
HBS_LGE_GEO_REGION=europe/ukraine
# HBS_LGE_GEO_REGION=africa/south-sudan

# old_dir=$(pwd)
current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`


unset OMP_NUM_THREADS
MAX_CORES=$(nproc)
$HBS_TEST_DIR/bin/extract.sh  time_perf 16         $HBS_LGE_GEO_REGION car
$HBS_TEST_DIR/bin/contract.sh time_perf 16	   $HBS_LGE_GEO_REGION car
$HBS_TEST_DIR/bin/extract.sh  time_perf $MAX_CORES $HBS_LGE_GEO_REGION car
$HBS_TEST_DIR/bin/contract.sh time_perf $MAX_CORES $HBS_LGE_GEO_REGION car
$HBS_TEST_DIR/bin/routing.sh  time_perf 10         $HBS_LGE_GEO_REGION car

# cd $old_dir
