#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

current_dir=`dirname "${BASH_SOURCE-$0}"`
HBS_TEST_DIR=`cd "${current_dir}/.."; pwd`

HBS_METRIC_FUNC=${1:-time_perf}
NREPEATS=${2:-10}

HBS_DATASET=${3:-africa}
OSRM_PROFILE=${4:-car}

# Collect measurements
unset OMP_NUM_THREADS
MAX_NPROC=$(nproc)
for i in $(seq 1 $NREPEATS); do
    echo "Iteration $i"
    for NCORES in 1 4 8 16 24 32 48 64 $(nproc); do
	if [ $NCORES -le $MAX_NPROC ]; then
	    echo "  -- ncores=$NCORES"
	    $HBS_TEST_DIR/bin/routing_single.sh $HBS_METRIC_FUNC $NCORES $HBS_DATASET $OSRM_PROFILE
	fi
    done
done

# Compress output files and convert the data to CSV
hbs_output_dir=${HBS_OUTPUT_DIR:-$HBS_TEST_DIR/results/${HBS_DATASET}}

hbs_testbed=$(basename $(dirname $(spack location -r)))
hbs_kernel=$(basename "$HBS_TEST_DIR")
hbs_dataset=$HBS_DATASET
hbs_testcase=0
hbs_timestamp=$(date '+%Y%d%m%H%M%S')

files_pattern=$hbs_output_dir/*/extract_routes-*-*.err
for HBS_SUBKERNEL in triangle_pruning compute_table ; do
    hbs_subkernel=$HBS_SUBKERNEL
    cat $files_pattern | grep "BENCH:" | grep $hbs_subkernel | cut -d":" -f2,3 | \
	while read line ; do echo "$hbs_testbed,${line%%:*},$hbs_kernel,$hbs_subkernel,$hbs_dataset,$hbs_testcase,$hbs_timestamp,${line##*:}" ; \
	done >> $hbs_output_dir/hbs_$hbs_subkernel.csv
done
rm -rf $files_pattern
rm -rf $hbs_output_dir/*/extract_routes-*-*.log
