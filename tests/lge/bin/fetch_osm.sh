#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

GEO_REGION=${1:-africa/south-sudan}

current_dir=`dirname "$0"`
root_dir=`cd "${current_dir}/.."; pwd`
data_dir=$root_dir/data

WORK_DIR=${data_dir}/${GEO_REGION}
OSM_FILE_BASENAME=map

mkdir -p ${WORK_DIR}
wget https://download.geofabrik.de/${GEO_REGION}-latest.osm.pbf -O $WORK_DIR/$OSM_FILE_BASENAME.osm.pbf
