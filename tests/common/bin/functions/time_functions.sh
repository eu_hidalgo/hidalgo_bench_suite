#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

# set -u
# this="${BASH_SOURCE-$0}"
# time_func_bin=$(cd -P -- "$(dirname -- "$this")" && pwd -P)

hbs_header() {
    local hbs_testbed=${HBS_TESTBED:-$(basename "$HBS_TEST_DIR")}
    local hbs_ncores=${HBS_NCORES:-$(nproc)}

    local hbs_kernel=${HBS_KERNEL:-$(basename "$HBS_TEST_DIR")}
    local hbs_subkernel=${HBS_SUBKERNEL:-0}
    local hbs_dataset=${HBS_DATASET:-default}
    local hbs_testcase=${HBS_TESTCASE:-0}
    local hbs_timestamp=${HBS_TIMESTAMP:-$(date '+%Y%d%m%H%M%S')}

    echo -n "$hbs_testbed,$hbs_ncores,$hbs_kernel,$hbs_subkernel,$hbs_dataset,$hbs_testcase,$hbs_timestamp"
}

hbs_time() {
    # hbs_testbed -> $(basename $(dirname $(spack location -r)))
    local hbs_time_format=${HBS_TIME_FORMAT:-"%x,%e,%M,%I,%O"}

    local hbs_timestamp=${HBS_TIMESTAMP:-$(date '+%Y%d%m%H%M%S')}

    local hbs_output_dir=${HBS_OUTPUT_DIR:-$HBS_TEST_DIR/results/${HBS_DATASET}}
    local hbs_time_file=${HBS_TIME_FILE:-$hbs_output_dir/hbs_time.csv}
    local hbs_time_stdout_file=${HBS_TIME_STDOUT_FILE:-$hbs_output_dir/time/$(basename $1)-time-$hbs_timestamp.log}
    local hbs_time_stderr_file=${HBS_TIME_STDERR_FILE:-$hbs_output_dir/time/$(basename $1)-time-$hbs_timestamp.err}

    mkdir -p $hbs_output_dir
    mkdir -p $hbs_output_dir/time
    HBS_TIMESTAMP=$hbs_timestamp echo -n "$(hbs_header)," >> $hbs_time_file
    # Option --quiet is supported not by all implementations of /usr/bin/time
    /usr/bin/time -f "$hbs_time_format" -o $hbs_time_file -a \
		  $@ > $hbs_time_stdout_file 2> $hbs_time_stderr_file
}

hbs_perf() {
    local hbs_prof_metrics=('cycles' 'instructions')
    if [ ! -z "$HBS_PROF_METRICS" ]; then
	hbs_prof_metrics=( "${HBS_PROF_METRICS[@]}" )
    fi

    local hbs_timestamp=${HBS_TIMESTAMP:-$(date '+%Y%d%m%H%M%S')}

    local hbs_output_dir=${HBS_OUTPUT_DIR:-$HBS_TEST_DIR/results/${HBS_DATASET}}
    local hbs_perf_file=${HBS_PERF_FILE:-$hbs_output_dir/hbs_perf.csv}
    local hbs_perf_stats_file=${HBS_PERF_STDOUT_FILE:-$hbs_output_dir/perf/$(basename $1)-perf-$hbs_timestamp.perf}
    local hbs_perf_stdout_file=${HBS_PERF_STDOUT_FILE:-$hbs_output_dir/perf/$(basename $1)-perf-$hbs_timestamp.log}
    local hbs_perf_stderr_file=${HBS_PERF_STDERR_FILE:-$hbs_output_dir/perf/$(basename $1)-perf-$hbs_timestamp.err}

    mkdir -p $hbs_output_dir
    mkdir -p $hbs_output_dir/perf

    HBS_TIMESTAMP=$hbs_timestamp hbs_header >> $hbs_perf_file

    perf stat -o $hbs_perf_stats_file -x, -d $@ > $hbs_perf_stdout_file 2> $hbs_perf_stderr_file
    for metric in "${hbs_prof_metrics[@]}"; do
	echo -n ",$(cat $hbs_perf_stats_file | grep ",$metric" | cut -d, -f1)" >> $hbs_perf_file
    done
    echo >> $hbs_perf_file
}

hbs_mpip() {
    local hbs_timestamp=${HBS_TIMESTAMP:-$(date '+%Y%d%m%H%M%S')}

    local hbs_output_dir=${HBS_OUTPUT_DIR:-$HBS_TEST_DIR/results/${HBS_DATASET}}
    local hbs_mpip_file=${HBS_MPIP_FILE:-$hbs_output_dir/hbs_mpip.csv}
    local hbs_mpip_stats_file=${HBS_MPIP_STDOUT_FILE:-$hbs_output_dir/mpip/$(basename $1)-mpip-$hbs_timestamp.csv}
    local hbs_mpip_stdout_file=${HBS_MPIP_STDOUT_FILE:-$hbs_output_dir/mpip/$(basename $1)-mpip-$hbs_timestamp.log}
    local hbs_mpip_stderr_file=${HBS_MPIP_STDERR_FILE:-$hbs_output_dir/mpip/$(basename $1)-mpip-$hbs_timestamp.err}

    mkdir -p $hbs_output_dir
    mkdir -p $hbs_output_dir/mpip
    HBS_TIMESTAMP=$hbs_timestamp echo -n "$(hbs_header)," >> $hbs_time_file

    export MPIP="-t 10.0 -k 2 -c -r"
    LD_PRELOAD=$MPIP_PREFIX/lib/libmpiP.so $@  2>&1 | tee $hbs_mpip_stdout_file #> $hbs_mpip_stdout_file 2> $hbs_mpip_stderr_file

    filename_line=`cat $hbs_mpip_stdout_file | grep Storing`
    if [ ! -z "$filename_line" ]; then
	mpiP_out=`echo 'print("'$filename_line'".split("[")[1].split("]")[0])' |python`
	mpi_time=`cat $mpiP_out  |grep Aggregate |head -n 1 | awk '{print $4}'`
	echo -n "$mpi_time" >> $hbs_mpip_file
    fi
    echo >> $hbs_mpip_file
}

hbs_time_perf() {
    hbs_time $@
    hbs_perf $@
}

hbs_perf_time() {
    hbs_perf $@
    hbs_time $@
}

hbs_all() {
    hbs_time $@
    hbs_perf $@
    hbs_mpip $@
}
