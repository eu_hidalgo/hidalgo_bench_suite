#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko <gogolenko@hlrs.de>
##
############################################################

from pathlib import Path
import pandas

class HBSReader:
    def __init__(self, folder = '.'):
        self.path=Path(folder)
        self.header_columns = "hbs_testbed", "hbs_ncores", "hbs_kernel", "hbs_subkernel", "hbs_dataset", "hbs_testcase", "hbs_timestamp"
        self.time_columns = "exit_status", "elapsed_time", "memory", "inputs", "outputs"
        self.perf_columns = "instructions", "cycles"
        self.mpip_columns = ['mpip']

    def read_for_tool(self, tool='time', agg=None, **kwargs):
        measures_files = list(self.path.rglob("hbs_{}.csv".format(tool)))
        tools_columns = list(eval('self.{}_columns'.format(tool)))
        names = list(self.header_columns) + tools_columns
        measurements=pandas.concat((pandas.read_csv(_, index_col=None, names=names, sep=',', comment='#') for _ in measures_files), axis=0, ignore_index=True)
        measurements=self.filter(measurements, **kwargs)
        return measurements if agg is None else self.agg(measurements, method=agg)
    
    def filter(self, measurements, **kwargs):
        for _k, _v in kwargs.items():
            if _k in self.header_columns and _k in measurements.columns:
                measurements=measurements[measurements[_k] == _v].drop(columns=[_k])
        return measurements
                
    def agg(self, measurements, method='mean', agg_fields = None):
        tools_columns=list(set(measurements.columns) - set(self.header_columns))
        agg_fields_list = ['hbs_timestamp']
        agg_fields_list.extend(agg_fields or [])
        group_fields_list = [_ for _ in self.header_columns if _ in measurements.columns and _ not in set(agg_fields_list)]
        return measurements.groupby(group_fields_list, as_index=False).agg({ _: method for _ in tools_columns})

    def time(self, agg=None, **kwargs):
        measurements = self.read_for_tool(tool='time', agg=None, **kwargs)
        measurements=measurements[measurements.exit_status == 0].drop(columns=['exit_status'])
        return measurements if agg is None else self.agg(measurements, method=agg)

    def perf(self, agg=None, **kwargs):
        measurements = self.read_for_tool(tool='perf', agg=None, **kwargs)
        measurements['ipc']=measurements['instructions']/measurements['cycles']
        return measurements if agg is None else self.agg(measurements, method=agg)

    def mpip(self, **kwargs):
        return self.read_for_tool(tool='mpip', **kwargs)
    
    def add_tool(self, name, col_names = None):
        import types
        setattr(self, "{}_columns".format(name), col_names or [])
        def new_tool(self, **kwargs):
            return self.read_for_tool(tool=name, **kwargs)
        setattr(self, name, types.MethodType( new_tool, self))
        #         setattr(self, name, types.MethodType( lambda self: self.read_for_tool(tool=name), self ))
        return self

def read_summary_data(reader, cpu=True, **kwargs):
    def clean_data(inputs):
        df=reader.agg(inputs, 'mean').sort_values(by='hbs_ncores')
        testbed_or_16_cores = (df['hbs_ncores']==16) | \
            ((df['hbs_testbed']=='intel8268') & (df['hbs_ncores']==48)) | \
            ((df['hbs_testbed']=='amd7763') & (df['hbs_ncores']==128))  | \
            ((df['hbs_testbed']=='armHi1620') & (df['hbs_ncores']==96))
        return df[testbed_or_16_cores]

    if cpu:
        df_perf=clean_data(reader.perf(**kwargs))
        df_time=clean_data(reader.time(**kwargs))
    else:
        df_perf=reader.perf(agg='mean', **kwargs)
        df_time=reader.time(agg='mean', **kwargs)

    df = pandas.merge(df_time, df_perf, on=list(set(reader.header_columns) - set(kwargs) - set(['hbs_timestamp'])))
    if cpu:
        hue = (df['hbs_ncores']==16)
        core_usage = list(map(lambda x: "16" if x==True else "Full\nnode",hue))
        df['core_usage']=core_usage
    return df
