#!/bin/bash
############################################################
## @file
## @copyright (C) 2021
##    All rights reserved.
##
## Use, modification, and distribution is subject to the license.
##
## @author Sergiy Gogolenko    <gogolenko@hlrs.de>
##         Nikela Papadopoulou <nikela@cslab.ece.ntua.gr>
##
############################################################
import matplotlib.pyplot as plt
import sys
import pandas
import numpy
import matplotlib.pyplot as plt
import seaborn

metric2label = {
    "hbs_testbed": 'Testbed name',
    "hbs_ncores":   'Number of cores',
    "hbs_kernel":   'Kernel',
    "hbs_subkernel":'Subkernel',
    "hbs_dataset":  'Dataset',
    "hbs_testcase": 'Testcase',
    "hbs_timestamp":'Timestamp',
    "elapsed_time": 'Elapsed real time (s)', #%E
    "outputs":      '#Filesystem outputs', #%O
    "inputs":       '#Filesystem inputs', #%I
    "memory":       'Maximum resident set (KB)', #%M - https://en.wikichip.org/wiki/resident_set_size
    "instructions": 'Instructions',
    "cycles": 'CPU cycles',
    "ipc": 'Instructions per Cycle',
    'core_usage': 'Core usage'
}

def prep_sns():
    seaborn.set_style("whitegrid")
    seaborn.set_context("paper")
    #Color palette
    cp = []
    cp.append("#aa585f")
    cp.append("#878581")
    cp.append("#24477C")
    cp.append("#71a936")
    cp.append("#ebd8a7")
    cp.append("#5a2c58")
    cp = cp*2
    seaborn.set_palette(cp)
    return seaborn

def plot_summary_data(df, xlabel='core_usage', core_plots=False):
    #Create 2-by-3 subplot
    if core_plots:
        f, axes = plt.subplots(1,3, figsize=(6, 2))
        axes1 = axes
    else:
        f, axes = plt.subplots(2,3, figsize=(6, 4))
        axes1 = axes[0]
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.8,hspace=0.3)

    #Iterate first row of subplots
    # for y in ['Elapsed real time (s)', 'Maximum resident set (KB)', 'Instructions per Cycle']:
    for i, y in enumerate(['elapsed_time', 'memory', 'ipc']):
        fig = seaborn.barplot(x=xlabel, y = y, hue='hbs_testbed', data=df.sort_values(by='hbs_testbed'), ax=axes1[i])
        fig.set(xlabel=metric2label[xlabel], ylabel=metric2label[y])#, xticklabels=[])
        fig.legend([],[],frameon=False)
        fig.set_xlabel(None)

        #Keep legend only for lower middle figure
        if core_plots:
            if i != 1:
                fig.legend([],[],frameon=False)
            else:
                fig.legend(loc=[-1.5,-0.5], ncol=3, frameon=False)

    if not core_plots:
        #Iterate second row of subplots    
        for i, y in enumerate(['', 'inputs', 'outputs' ]):
            if y in df:
                fig = seaborn.barplot(x=xlabel, y = y, hue='hbs_testbed',data=df.sort_values(by='hbs_testbed'), ax=axes[1,i])
                fig.set(xlabel=metric2label[xlabel], ylabel=metric2label[y])
                fig.set_xlabel(None)

                #Keep legend only for lower middle figure
                if i != 1:
                    fig.legend([],[],frameon=False)
                else:
                    fig.legend(loc=[-1.5,-0.5], ncol=3, frameon=False)

def scalability_errorbar(ax, reader, data, testbed, metric = 'elapsed_time', x_axis='hbs_ncores'):
    mean = reader.agg(data, 'mean').sort_values(by=x_axis)
    err  = reader.agg(data, 'std').sort_values(by=x_axis)
    df = pandas.DataFrame({'x_axis' : mean[x_axis], 'mean' : mean[metric], 'std': err[metric]})
    ax.errorbar(df['x_axis'], df['mean'], yerr=df['std'], label=testbed)

def scalability_plot(ax, reader, measurements, metric = 'elapsed_time', x_axis='hbs_ncores', groupby='hbs_testbed'):
    for testbed, data in measurements.groupby(groupby):
        scalability_errorbar(ax, reader, data, testbed, metric, x_axis)
    ax.set_xlabel(metric2label[x_axis])
    ax.set_ylabel(metric2label[metric])
    xticks = sorted(measurements[x_axis].unique())
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticks)
